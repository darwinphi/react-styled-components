import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import styled, { injectGlobal } from 'styled-components'
import reset from 'styled-reset'

// import css from './index.css'

const baseStyles = () => injectGlobal`
	${reset}
	@import url('https://fonts.googleapis.com/css?family=Roboto');
`

const Greet = styled.h1`
	margin: 0;
	padding: 0;
	flex: 1;
	background: papayawhip;
	border-radius: 3px;
	margin: 1em;
	padding: 1em;
	font-family: 'Roboto', sans-serif;
`

const Container = styled.div`
	display: flex;
	width: 100vw;
`

const Button = styled.button`
  background: ${props => props.primary ? 'palevioletred': 'white'};
  border-radius: 3px;
  border: none;
  color: ${props => props.primary ? 'white': 'palevioletred'}
  padding: 0.5em 1em 0.5em 1em;
`;

const TomatoButton = styled(Button)`
  background: tomato;
`;

class App extends Component {
	render() {
		baseStyles()
		return (
			<Container>
				<Greet>Hello, World</Greet>
				<Greet>Hello, World</Greet>
				<Greet>
					<Button primary>Send</Button>
				</Greet>
			</Container>
		)
	}
}

ReactDOM.render(<App />, document.getElementById('app'))